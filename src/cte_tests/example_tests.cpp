#include "cte/cte.hpp"

#include <iostream>
#include <numeric>
#include <memory>

#include "gtest/gtest.h"


///////////////////////////////////////////////
// Auxiliary Functions
///////////////////////////////////////////////

std::shared_ptr<ConvexTransformationEstimation::Point2D>
get_transformed_point_2d(double x,
                         double y,
                         double theta=30,
                         double x_diff=20,
                         double y_diff=-10)
{
  double DEG_TO_RAD = 3.14159/180.;
  double c = cos(theta*DEG_TO_RAD);
  double s = sin(theta*DEG_TO_RAD);

  double new_x = x*c + y*s - (c*x_diff + s*y_diff);
  double new_y = x*(-s) + y*c - (c*y_diff - s*x_diff);

  return std::make_shared<ConvexTransformationEstimation::Point2D>(new_x, new_y);
}

////////////////////////////////////////////////
// Example
////////////////////////////////////////////////

TEST(Example, Example ) {

  // 1. The first step is to create a problem definition object.
  // There are two of these problem types supported in the current implementation:
  // a. GraphAlignmentProblem2D
  // b. GraphAlignmentProblem3DKnownDepth
  // The constructors for these objects take the following:
  //    i.   A vector of shared_ptrs to query feature points positions
  //    ii.  A vector of shared_ptrs to reference feature point positions
  //    iii. A std::function<double(int j, int i)> object that returns the dissimilarity
  //           between the j-th query feature point and the i-th reference feature point
  //
  // Additionally, the GraphAlignmentProblem3DKnownDepth constructor takes the following:
  //    iv.  A maximum depth difference threshold that can be used to limit what features
  //            are matched

  // Here we provide an example of how to generate these vectors of points.

  // In the 3DKnownDepth case, this would be Point3D
  typedef ConvexTransformationEstimation::Point2D Point_T;
  std::vector< std::shared_ptr<Point_T> > points(35);
  int i = 0; double x = -2;

  // In the 3DKnownDepth case, these points would have a third parameter for z
  points[i+0] = std::make_shared<Point_T>(x, 0.1);
  points[i+1] = std::make_shared<Point_T>(x, 0);
  points[i+2] = std::make_shared<Point_T>(x, 0.5);
  points[i+3] = std::make_shared<Point_T>(x, 1.5);
  points[i+4] = std::make_shared<Point_T>(x, 2.5);
  points[i+5] = std::make_shared<Point_T>(x, 4);
  points[i+6] = std::make_shared<Point_T>(x, 5);
  i = 7; x = -1;
  points[i+0] = std::make_shared<Point_T>(x, 0.15);
  points[i+1] = std::make_shared<Point_T>(x, 0);
  points[i+2] = std::make_shared<Point_T>(x, 0.5);
  points[i+3] = std::make_shared<Point_T>(x, 1.5);
  points[i+4] = std::make_shared<Point_T>(x, 2.5);
  points[i+5] = std::make_shared<Point_T>(x, 4);
  points[i+6] = std::make_shared<Point_T>(x, 5);
  i = 14; x = 0;
  points[i+0] = std::make_shared<Point_T>(x, 0.2);
  points[i+1] = std::make_shared<Point_T>(x, 0);
  points[i+2] = std::make_shared<Point_T>(x, 0.5);
  points[i+3] = std::make_shared<Point_T>(x, 1.5);
  points[i+4] = std::make_shared<Point_T>(x, 2.5);
  points[i+5] = std::make_shared<Point_T>(x, 4);
  points[i+6] = std::make_shared<Point_T>(x, 5);
  i = 21; x = 1;
  points[i+0] = std::make_shared<Point_T>(x, 0.15);
  points[i+1] = std::make_shared<Point_T>(x, 0);
  points[i+2] = std::make_shared<Point_T>(x, 0.5);
  points[i+3] = std::make_shared<Point_T>(x, 1.5);
  points[i+4] = std::make_shared<Point_T>(x, 2.5);
  points[i+5] = std::make_shared<Point_T>(x, 4);
  points[i+6] = std::make_shared<Point_T>(x, 5);
  i = 28; x = 2;
  points[i+0] = std::make_shared<Point_T>(x, 0.1);
  points[i+1] = std::make_shared<Point_T>(x, 0);
  points[i+2] = std::make_shared<Point_T>(x, 0.5);
  points[i+3] = std::make_shared<Point_T>(x, 1.5);
  points[i+4] = std::make_shared<Point_T>(x, 2.5);
  points[i+5] = std::make_shared<Point_T>(x, 4);
  points[i+6] = std::make_shared<Point_T>(x, 5);

  std::vector< std::shared_ptr<Point_T> > points2(21);
  i = 0; x = -1;
  points2[i+0] = get_transformed_point_2d(x, 0.15);
  points2[i+1] = get_transformed_point_2d(x, 0);
  points2[i+2] = get_transformed_point_2d(x, 0.5);
  points2[i+3] = get_transformed_point_2d(x, 1.5);
  points2[i+4] = get_transformed_point_2d(x, 2.5);
  points2[i+5] = get_transformed_point_2d(x, 4);
  points2[i+6] = get_transformed_point_2d(x, 5);
  i = 7; x = 0;
  points2[i+0] = get_transformed_point_2d(x, 0.2);
  points2[i+1] = get_transformed_point_2d(x, 0);
  points2[i+2] = get_transformed_point_2d(x, 0.5);
  points2[i+3] = get_transformed_point_2d(x, 1.5);
  points2[i+4] = get_transformed_point_2d(x, 2.5);
  points2[i+5] = get_transformed_point_2d(x, 4);
  points2[i+6] = get_transformed_point_2d(x, 5);
  i = 14; x = 1;
  points2[i+0] = get_transformed_point_2d(x, 0.15);
  points2[i+1] = get_transformed_point_2d(x, 0);
  points2[i+2] = get_transformed_point_2d(x, 0.5);
  points2[i+3] = get_transformed_point_2d(x, 1.5);
  points2[i+4] = get_transformed_point_2d(x, 2.5);
  points2[i+5] = get_transformed_point_2d(x, 4);
  points2[i+6] = get_transformed_point_2d(x, 5);

  // Here we define a function that calculates dissimilarity between features
  // In many cases this could just be the l2 distance between feature vectors.
  // We simulate that here using hard coded feature vectors of dimension 1.
  std::function<double(int, int)> diss_function = [=](int j, int i) {
    typedef std::chrono::high_resolution_clock myclock;
    unsigned seed = std::chrono::duration_cast<std::chrono::microseconds>(
        std::chrono::system_clock::now().time_since_epoch()).count();

    std::mt19937_64 rng(seed);
    std::uniform_real_distribution<double> unif(-0.05,0.05);

    std::vector< double> feature_i =
    {0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4,
     0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
     0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2,
     0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
     0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4};

    std::vector< double> feature_j =
    {0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4,
     0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26,
     0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2,
     0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26,
     0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4};


    std::mt19937_64 rng_i(i);
    std::uniform_real_distribution<double> unif_i(-0.05,0.05);  
    double featurei = feature_i[i] + unif_i(rng_i);;

    std::mt19937_64 rng_j(j);
    std::uniform_real_distribution<double> unif_j(-0.05,0.05);
    int trajectory_offset = 7;
    double featurej = feature_i[j+trajectory_offset] + unif_j(rng_j);;  
    
    return pow(featurei - featurej, 2.0);
  };

  // Here we create the problem definition
  // In the GraphAlignment3DKnownDepth case, we could add an additional
  // threshold parameter such that only points at about the same depth
  // are compared during the alignment process
  ConvexTransformationEstimation::GraphAlignmentProblem2D problem(
      points2,
      points,
      diss_function);


  
  // 2. The second step is to create a solver for the type of transformation
  //    that is desired.
  // The following solvers are supported:
  // a.   SolverAffine
  // b.   SolverSymmetric
  // c.   SolverRigidBody (uses the linear approximation proposed in the associated paper)
  // d.   SolverRigidBody_SOConvHull (is also included for comparison)
  //
  // The first parameter is the number of iterations.
  //   The linear optimization problem is repetitively solved using a smaller region
  //   of interest centered on the solution from the prior iteration.
  // The region of interest at time step t_j is 2/3 the size of the region of interest 
  //   at time step t_(j-1)
  // The RigidBody cases also have a second boolean parameter that specifies whether
  //   or not to round the final solution if it is not a valid rotation.   

  ConvexTransformationEstimation::SolverRigidBody solver(3, false);


  
  // 3. Finally we solve the problem
  // This returns a SolverResults object with the following useful functions:
  // get_R() - returns a vector of doubles representing R in row major order
  // get_t() - returns a vector of doubles representing t in order x, y, [z]
  // has_valid_rotation_matrix() - checkes if R is approximatelly valid rotation
  // print() - prints the solution to std::cout
  auto result = solver.solve_problem(problem);
  result.print();
}
