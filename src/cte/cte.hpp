#ifndef __CONVEX_TRANSFORMATION_ESTIMATION_HPP
#define __CONVEX_TRANSFORMATION_ESTIMATION_HPP 

#include <vector>
#include <functional>
#include <memory>
#include <thread>
#include <mutex>
#include <queue>

#include <fusion.h>

namespace ConvexTransformationEstimation {

////////////////////////////////////////////////////////////////////////////////
// Point Storage Classes
////////////////////////////////////////////////////////////////////////////////

class Point
{
 protected:
  double _x;
  double _y;
 public:
  Point(double x, double y):
      _x(x),
      _y(y)
  {}

  virtual ~Point(){}

  double x() const {return this->_x;} 
  double y() const {return this->_y;}
  virtual std::vector<double> coordinates()
  {
    std::vector<double> coords {this->_x, this->_y};
    return coords;
  }
};

typedef Point Point2D;

class Point3D : public Point
{
 private:
  double _z;

 public:
  Point3D(double x, double y, double z):
      Point(x, y),
      _z(z)
  {}

  ~Point3D(){}

  double z() const {return this->_z;}
  std::vector<double> coordinates() const
  {
    std::vector<double> coords {this->_x, this->_y, this->_z};
    return coords;
  }
};

////////////////////////////////////////////////////////////////////////////////
// Sparse Feature Alignment Solver Results Class
////////////////////////////////////////////////////////////////////////////////

class GraphAlignmentProblem;

class SolverResults
{
 private:
  bool empty_result;
  int dim;

  // Transformation matrix in row major order
  std::vector< double > R;
  // Translation in order: x, y, z
  std::vector< double > t; 
  // Local deviations d[j][k] where j refers to d_j^(k)
  // This is empty if local deviations were not enabled
  std::vector< std::vector<double> > d; 
  int rotation_dim;

  int num_ignored_query_points;

  double solution_value;
  
 public:
  SolverResults():
      empty_result(true),
      dim(0),
      R(),
      t(),
      d(),
      rotation_dim(0),
      num_ignored_query_points(0),
      solution_value(std::numeric_limits<double>::max())
  {}

  SolverResults(int dim_,
                std::vector<double> R_,
                std::vector<double> t_,
                int num_ignored_query_points_,
                double solution_value_,                                
                std::vector< std::vector<double> > d_={{}}):
      empty_result(false),
      dim(dim_),
      R(R_),
      t(t_),
      d(d_),
      rotation_dim(dim_),
      num_ignored_query_points(num_ignored_query_points_),
      solution_value(solution_value_)
  {
    assert(dim == 2 || dim == 3);
    assert(R.size() ==  dim*dim);
    assert(t.size() == dim);
  }
  
  SolverResults(int dim_,
                std::vector<double> R_,
                std::vector<double> t_,
                int rotation_dim_,
                int num_ignored_query_points_,
                double solution_value_,                
                std::vector< std::vector<double> > d_={{}}):
      empty_result(false),
      dim(dim_),
      R(R_),
      t(t_),
      rotation_dim(rotation_dim_),
      num_ignored_query_points(num_ignored_query_points_),
      solution_value(solution_value_)      
  {
    assert(dim == 2 || dim == 3);
    assert(R.size() ==  dim*dim);
    assert(t.size() == dim);
  }

  // 
  bool is_empty() const {return this->empty_result;}
  int get_Dim() const { return this->dim; }
  std::vector<double> get_R() const { return this->R; }
  std::vector<double> get_t() const { return this->t; }

  std::vector< std::vector<double> > get_d() const { return this->d; }
  bool has_d() const {return ((this->d.size() != 0) && (this->d[0].size() != 0));}
  double get_d_jk(int j, int k) const {return this->d[j][k]; }

  bool has_valid_rotation_matrix() const;
  bool has_orthogonal_rotation_matrix() const;
  bool has_correct_determinant() const;

  void round_R();

  void print()
  {
    std::cout << "Dimension: " << this->dim << "\n";
    if(this->dim == 2)
    {
      std::cout << "[R_00, R_01,\n R_10, R_11]:\n[" <<
          this->R[0] << ", " << this->R[1] << ",\n " << 
          this->R[2] << ", " <<this->R[3] << "]\n";
  
      std::cout << "[t_0, t_1]:\n[" <<
          this->t[0] << ", " << this->t[1] << "]\n";
    }
    else
    {
      std::cout << "[R_00, R_01, R_02,\n R_10, R_11, R_12,\n R_20, R_21, R_22]:\n[" <<
          this->R[0] << ", " << this->R[1] << ", " << this->R[2] << ",\n " <<
          this->R[3] << ", " << this->R[4] << ", " << this->R[5] << ",\n " <<
          this->R[6] << ", " << this->R[7] << ", " << this->R[8] << "]\n";


      std::cout << "[t_0, t_1, t_2]:\n[" <<
          this->t[0] << ", " << this->t[1] << ", " << this->t[2] << "]\n";
    }
  }

  static void print_unscaled_and_shifted_result(const SolverResults& result,
                                                const GraphAlignmentProblem& problem);
  
  static void unscale_and_shift_results(SolverResults& result,
                                        const GraphAlignmentProblem& problem);

  int get_num_ignored_query_points() const {return this->num_ignored_query_points;}
  double get_solution_value() const {return this->solution_value;}
};


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
//
//  Problem Definition Classes
//
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// GraphAlignmentProblem Base Class
////////////////////////////////////////////////////////////////////////////////

class GraphAlignmentProblem
{

  // Variable Members
 protected:
  int _num_query_points;
  int _num_reference_points;

  int _dimension;
  
  // Dissimilarity values
  // dissimilarity_values[j][i] is the dissimilarity of the
  // j-th remote query point and the i-th local reference point
  std::vector< std::vector<double> > _dissimilarity_values;

  double _min_dissimilarity;
  double _max_dissimilarity;

  bool _enable_local_deviations;

  // Function Members
 protected:

 public:
  double _min_dimension;
  double _max_dimension;

  // Returns a vector of vectors containing the dissimilarity values for each pair of
  // matched features
  // dissimilarity_values()[j][i] is the dissimilarity of the
  // j-th query point and the i-th remote point
  // if features not matched dissimilarity_values()[j][i] = -1
  const std::vector< std::vector<double> >& dissimilarity_values() const {return this->_dissimilarity_values;}
  
  double shift_and_scale_coord(double coord) const
  {
    double desired_max = 1.0;
    return (coord - this->_min_dimension) *
        (desired_max/(this->_max_dimension - this->_min_dimension));
  }

  double undo_shift_and_scale_coord(double coord) const
  {
    double desired_max = 1.0;
    return (coord / (desired_max/(this->_max_dimension - this->_min_dimension))) +
        this->_min_dimension;
  }
  
 private:
  // TODO: Need to make copy constructor/assignment operator private
  
 public:
  GraphAlignmentProblem(int num_query_points,
                        int num_reference_points,
                        int dimension,
                        bool enable_local_deviations=false):
  _num_query_points(num_query_points),
  _num_reference_points(num_reference_points),
  _dimension(dimension),
  _dissimilarity_values(num_query_points),
  _min_dissimilarity(std::numeric_limits<double>::max()),
  _max_dissimilarity(std::numeric_limits<double>::min()),
  _min_dimension(0),
  _max_dimension(1),  
  _enable_local_deviations(enable_local_deviations)
  {}
  
  virtual ~GraphAlignmentProblem() {}

  // Returns a list of the coeficients of the lower convex hull
  // planar functions for each of the query points
  //
  // get_lower_convex_hull_plane_functions()[j][m] is the m-th vector of planar coeficients
  // for the j-th query point, ex:
  // {r_m, s_m, t_m} for 2D or
  // {q_m, r_m, s_m, t_m} for 3D
  virtual const std::vector< std::vector< std::vector<double> > >
  get_lower_convex_hull_plane_functions(
      int iteration_number,
      double trust_region_size,
      SolverResults prior_result) const = 0;

  // Accessor Functions
  virtual const std::vector< std::shared_ptr<Point> > query_feature_point_locations() const = 0;
  virtual const std::vector< std::shared_ptr<Point> > reference_feature_point_locations() const = 0;

  int num_query_points() const {return this->_num_query_points;}
  int num_reference_points() const {return this->_num_reference_points;}
  int dimension() const {return this->_dimension;}
  double get_min_dissimilarity() const {return this->_min_dissimilarity;}
  double get_max_dissimilarity() const {return this->_max_dissimilarity;}

  // Functions used for determining what transformation to estimate
  // and how to estimate it
  virtual bool transformation_restricted_to_xy_plane() const = 0;
  virtual bool fix_z() const {return false;}

  // This returns the maximum local deviations allowed
  bool local_deviations_allowed() const {return this->_enable_local_deviations;}
  virtual const std::vector<double> max_local_deviations() const = 0;

  virtual const
  std::pair<std::vector<double>, std::vector<double> > get_max_and_min_reference_dims() const = 0;
};

////////////////////////////////////////////////////////////////////////////////
// 2D Case
////////////////////////////////////////////////////////////////////////////////

class GraphAlignmentProblem2D : public GraphAlignmentProblem
{
  // Variable Members
 private:
  
  // Query point locations
  std::vector< std::shared_ptr<Point2D> > _query_feature_point_locations;

  // Reference point locations
  std::vector< std::shared_ptr<Point2D> > _reference_feature_point_locations;

  // Dissimilarity Function
  // A function pointer to a function that calculates the dissimilarity
  //     between the j-th query feature and the i-th reference feature  
  const std::function<double(int j, int i)> _dissimilarity_function;

  const std::vector<double> _max_deviations;
  // Function Members
 private:
  void calculate_dissimilarity_values();
  void calculate_min_max_dimension_values();
  
  std::vector< std::vector<double> >
  get_lower_convex_hull_plane_functions_for_xi_j(int j,
                                                 int iteration_number,
                                                 double trust_region_size,
                                                 SolverResults prior_result) const; 
  
 public:
  // GraphAlignmentProblem3D Constructor
  //
  // query_feature_point_locations - a vector of 2D query point locations in the query coordinate
  //     frame
  // reference_feature_point_locations - a vector of 2D reference point locations in the reference coordinate
  //     frame
  // dissimilarity_function - a function pointer to a function that calculates the dissimilarity
  //     between the j-th query feature and the i-th reference feature
  GraphAlignmentProblem2D(std::vector< std::shared_ptr<Point2D> >& query_feature_point_locations,
                          std::vector< std::shared_ptr<Point2D> >& reference_feature_point_locations,
                          std::function<double(int j, int i)> dissimilarity_function,
                          bool enable_local_deviations=false,
                          std::vector<double> max_deviations={0.5, 0.5}):
      GraphAlignmentProblem(query_feature_point_locations.size(),
                            reference_feature_point_locations.size(),
                            2,
                            enable_local_deviations),
      _query_feature_point_locations(query_feature_point_locations),
      _reference_feature_point_locations(reference_feature_point_locations),
      _dissimilarity_function(dissimilarity_function),
      _max_deviations(max_deviations)
  {
    calculate_dissimilarity_values();
    //calculate_min_max_dimension_values(); Disabled scaling        
  }

  ~GraphAlignmentProblem2D()
  {
    //All Objects Handled Automatically
  }

  const std::vector< std::vector< std::vector<double> > > get_lower_convex_hull_plane_functions(
      int iteration_number,
      double trust_region_size,
      SolverResults prior_result) const;

  const std::vector< std::shared_ptr<Point> > query_feature_point_locations() const
  {
    return this->_query_feature_point_locations;
  }
  const std::vector< std::shared_ptr<Point> > reference_feature_point_locations() const
  {
    return this->_reference_feature_point_locations;
  }

  bool transformation_restricted_to_xy_plane() const {return true;}
  const std::vector<double> max_local_deviations() const{return this->_max_deviations;}

  const std::pair<std::vector<double>, std::vector<double> > get_max_and_min_reference_dims() const;
};


////////////////////////////////////////////////////////////////////////////////
// 3D Case            
////////////////////////////////////////////////////////////////////////////////

class GraphAlignmentProblem3D : public GraphAlignmentProblem
{
  // Variable Members
 private:
  
  // Query point locations
  std::vector< std::shared_ptr<Point3D> > _query_feature_point_locations;

  // Reference point locations
  std::vector< std::shared_ptr<Point3D> > _reference_feature_point_locations;

  // Dissimilarity Function
  // A function pointer to a function that calculates the dissimilarity
  //     between the j-th query feature and the i-th reference feature  
  const std::function<double(int j, int i)> _dissimilarity_function;

  const std::vector<double> _max_deviations;
  
  // Function Members
 private:
  void calculate_dissimilarity_values();
  void calculate_min_max_dimension_values();
  
  std::vector< std::vector<double> >
  get_lower_convex_hull_plane_functions_for_xi_j(int j,
                                                 int iteration_number,
                                                 double trust_region_size,
                                                 SolverResults prior_result) const; 
  
 public:
  // GraphAlignmentProblem3D Constructor
  //
  // query_feature_point_locations - a vector of 2D query point locations in the query coordinate
  //     frame
  // reference_feature_point_locations - a vector of 2D reference point locations in the reference coordinate
  //     frame  
  // dissimilarity_function - a function pointer to a function that calculates the dissimilarity
  //     between the j-th query feature and the i-th reference feature
  GraphAlignmentProblem3D(std::vector< std::shared_ptr<Point3D> >& query_feature_point_locations,
                          std::vector< std::shared_ptr<Point3D> >& reference_feature_point_locations,
                          std::function<double(int j, int i)> dissimilarity_function,
                          bool enable_local_deviations=false,
                          std::vector<double> max_deviations={0.5, 0.5, 0.1}):
      GraphAlignmentProblem(query_feature_point_locations.size(),
                            reference_feature_point_locations.size(),
                            3,
                            enable_local_deviations),
      _query_feature_point_locations(query_feature_point_locations),
      _reference_feature_point_locations(reference_feature_point_locations),
      _dissimilarity_function(dissimilarity_function),
      _max_deviations(max_deviations)
  {
    calculate_dissimilarity_values();
    //calculate_min_max_dimension_values(); Disabled scaling
  }

  ~GraphAlignmentProblem3D()
  {
    //All Objects Handled Automatically
  }

  const std::vector< std::vector< std::vector<double> > > get_lower_convex_hull_plane_functions(
      int iteration_number,
      double trust_region_size,
      SolverResults prior_result) const;

  const std::vector< std::shared_ptr<Point> > query_feature_point_locations() const
  {
    auto query_points = std::vector< std::shared_ptr<Point> >();
    query_points.reserve(this->_query_feature_point_locations.size());
    for( auto p : this->_query_feature_point_locations)
    {
      query_points.push_back(std::static_pointer_cast<Point, Point3D>(p));
    }
    return query_points;
  }
  const std::vector< std::shared_ptr<Point> > reference_feature_point_locations() const
  {
    auto reference_points = std::vector< std::shared_ptr<Point> >();
    reference_points.reserve(this->_reference_feature_point_locations.size());
    for( auto p : this->_reference_feature_point_locations)
    {
      reference_points.push_back(std::static_pointer_cast<Point, Point3D>(p));
    }
    return reference_points;
  }
  bool transformation_restricted_to_xy_plane() const {return false;}
  const std::vector<double> max_local_deviations() const{return this->_max_deviations;}

  const std::pair<std::vector<double>, std::vector<double> > get_max_and_min_reference_dims() const;
};

////////////////////////////////////////////////////////////////////////////////
// 3D Known Depth Case
////////////////////////////////////////////////////////////////////////////////

class GraphAlignmentProblem3DKnownDepth : public GraphAlignmentProblem
{
  // Variable Members
 private:
  
  // Query point locations
  std::vector< std::shared_ptr<Point3D> > _query_feature_point_locations;

  // Reference point locations
  std::vector< std::shared_ptr<Point3D> > _reference_feature_point_locations;

  // Dissimilarity Function
  // A function pointer to a function that calculates the dissimilarity
  //     between the j-th query feature and the i-th reference feature  
  const std::function<double(int j, int i)> _dissimilarity_function;

  // Depth Threshold for pairing feature points
  const double _depth_difference_threshold;
  const bool _keep_z_fixed;

  const std::vector<double> _max_deviations;

  // Function Members
 private:
  void calculate_dissimilarity_values();
  void calculate_min_max_dimension_values();  
  
  std::vector< std::vector<double> >
  get_lower_convex_hull_plane_functions_for_xi_j(int j,
                                                 int iteration_number,
                                                 double trust_region_size,
                                                 SolverResults prior_result) const; 
  
 public:
  // GraphAlignmentProblem3D Constructor
  //
  // query_feature_point_locations - a vector of 2D query point locations in the query coordinate
  //     frame
  // reference_feature_point_locations - a vector of 2D reference point locations in the reference coordinate
  //     frame    
  // dissimilarity_function - a function pointer to a function that calculates the dissimilarity
  //     between the j-th query feature and the i-th reference feature
  // depth_threshold - the maximum depth difference between features that can be matched
  // keep_z_fixed - a boolean value specifying whether z should be fixed in the optimization
  GraphAlignmentProblem3DKnownDepth(std::vector< std::shared_ptr<Point3D> >& query_feature_point_locations,
                                    std::vector< std::shared_ptr<Point3D> >& reference_feature_point_locations,
                                    std::function<double(int j, int i)> dissimilarity_function,
                                    double depth_threshold,
                                    bool keep_z_fixed=true,
                                    bool enable_local_deviations=false,
                                    std::vector<double> max_deviations={0.5, 0.5, 0.1}):
      GraphAlignmentProblem(query_feature_point_locations.size(),
                            reference_feature_point_locations.size(),
                            3,
                            enable_local_deviations),
      _query_feature_point_locations(query_feature_point_locations),
      _reference_feature_point_locations(reference_feature_point_locations),
      _dissimilarity_function(dissimilarity_function),
      _depth_difference_threshold(depth_threshold),
      _keep_z_fixed(keep_z_fixed),
      _max_deviations(max_deviations)
  {
    calculate_dissimilarity_values();
    //calculate_min_max_dimension_values(); Disabled scaling    
  }

  ~GraphAlignmentProblem3DKnownDepth()
  {
    //All Objects Handled Automatically
  }

  const std::vector< std::vector< std::vector<double> > > get_lower_convex_hull_plane_functions(
      int iteration_number,
      double trust_region_size,
      SolverResults prior_result) const;

  const std::vector< std::shared_ptr<Point> > query_feature_point_locations() const
  {
    auto query_points = std::vector< std::shared_ptr<Point> >();
    query_points.reserve(this->_query_feature_point_locations.size());
    for( auto p : this->_query_feature_point_locations)
    {
      query_points.push_back(std::static_pointer_cast<Point, Point3D>(p));
    }
    return query_points;
  }

  const std::vector< std::shared_ptr<Point> > reference_feature_point_locations() const
  {
    auto reference_points = std::vector< std::shared_ptr<Point> >();
    reference_points.reserve(this->_reference_feature_point_locations.size());
    for( auto p : this->_reference_feature_point_locations)
    {
      reference_points.push_back(std::static_pointer_cast<Point, Point3D>(p));
    }
    return reference_points;
  }
  
  bool transformation_restricted_to_xy_plane() const {return true;}
  bool fix_z() const {return this->_keep_z_fixed;}
  const std::vector<double> max_local_deviations() const{return this->_max_deviations;}

  const std::pair<std::vector<double>, std::vector<double> > get_max_and_min_reference_dims() const;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Implementation of "Communication Constrained Trajectory Alignment for Multi-Agent Inspection via
//                    Linear Programing", Joshua G. Mangelson, Ram Vasudevan, and Ryan M. Eustice,
//                    IEEE OCEANS 2018, Charleston, South Carolina.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
// Sparse Feature Alignment Model Creator 
////////////////////////////////////////////////////////////////////////////////

class FeatureAlignmentConstraintData
{
 private:
  const mosek::fusion::Matrix::t constraint_matrix;
  const mosek::fusion::Matrix::t constant_offset_matrix;
  const std::vector<int> u_and_d_variables_to_constrain;
 public:
  FeatureAlignmentConstraintData(
      mosek::fusion::Matrix::t _constraint_matrix,
      mosek::fusion::Matrix::t _constant_offset_matrix,
      std::vector<int> _u_and_d_variables_to_constrain):
      constraint_matrix(_constraint_matrix),
      constant_offset_matrix(_constant_offset_matrix),
      u_and_d_variables_to_constrain(_u_and_d_variables_to_constrain)
  {}
    
  ~FeatureAlignmentConstraintData() {}

  const mosek::fusion::Matrix::t get_constraint_matrix() const {return constraint_matrix;}
  const mosek::fusion::Matrix::t get_constant_offset_matrix() const {return constant_offset_matrix;}
  const std::vector<int> get_variables_to_constrain() const {return u_and_d_variables_to_constrain;}
};

class FeatureAlignmentModelCreator
{
 private:
  
  // This function creates the problem variables
  // needed for the estimation problem
  static
  void create_variables_for_problem(
      mosek::fusion::Model::t model,
      const GraphAlignmentProblem & problem,
      const std::string name_extension="");

  // This function stacks the variables in a matrix "x" such that they
  // can be multiplied with the results of
  // construct_base_feature_alignment_constraint_matrix
  //
  // Let:
  // A = constraint_base_feature_alignment_constraint_matrix().first
  // b = constraint_base_feature_alignment_constraint_matrix().second
  //
  // Then:
  // A * x + b <= 0 is the constraint that should be enforced
  static
  mosek::fusion::Variable::t
  stack_variables_for_feature_alignment_constraint_matrix(
      mosek::fusion::Model::t model,
      const GraphAlignmentProblem & problem,
      const std::string variable_extension="");
  

 public:
  // This function creates the constraints matrix as an array that
  // can be saved for use multiple times
  static
  FeatureAlignmentConstraintData
  construct_base_feature_alignment_constraint_matrix(
      const GraphAlignmentProblem & problem,
      int iteration_number,
      double trust_region_size,
      SolverResults prior_result,
      bool print_to_file=false);

  // This function takes the constraint matrix data and
  // sets up the base feature alignment model
  static
  void setup_base_feature_alignment_model(
      const GraphAlignmentProblem & problem,
      mosek::fusion::Model::t model,
      const FeatureAlignmentConstraintData & constraint_data,
      const std::string variable_extension="");
};


////////////////////////////////////////////////////////////////////////////////
// L2 Norm Linear Approximation Base Class 
////////////////////////////////////////////////////////////////////////////////

class L2NormApproximation
{
 public:
  const int total_sections;  
  
 private:
  
  std::mutex thread_results_mutex;
  std::vector<SolverResults> thread_results;

  std::mutex queue_mutex;
  std::queue<int> items_to_process;

  int number_of_threads;
  int number_of_sections_per_thread;
  
 protected:
  const GraphAlignmentProblem & problem;
  std::mutex constraint_data_mutex;
  const FeatureAlignmentConstraintData & constraint_data;
  const std::function<void(mosek::fusion::Model::t,
                           const GraphAlignmentProblem &,
                           const std::string)>  enforce_transformation_structure;
  const int ignored_points;
  
 protected:
  
  void set_thread_result(int section_number,
                         SolverResults& result)
  {
    std::lock_guard<std::mutex> guard(thread_results_mutex);
    this->thread_results[section_number] = result;
  }

  int get_item_from_queue()
  {
    std::lock_guard<std::mutex> guard(queue_mutex);
    if(this->items_to_process.empty())
      return -1;

    int item = this->items_to_process.front();
    this->items_to_process.pop();
    return item;
  }

  std::vector<int> get_items_from_queue(int max_num_items)
  {
    std::lock_guard<std::mutex> guard(queue_mutex);
    std::vector<int> items;

    while(!this->items_to_process.empty() &&
          items.size() < max_num_items)
    {
      int item = this->items_to_process.front();
      this->items_to_process.pop();
      items.push_back(item);
    }
    return items;
  }

  void add_item_to_queue(int item)
  {
    std::lock_guard<std::mutex> guard(queue_mutex);
    this->items_to_process.push(item);
  }
  
  virtual void enforce_subproblem_constraints(mosek::fusion::Model::t model,
                                 int section_number) = 0;
  virtual void solve_subproblem(mosek::fusion::Model::t model,
                                int section_number) = 0;
  virtual void solve_subproblems(mosek::fusion::Model::t model,
                                 std::vector<int> section_numbers) = 0;  
  
 public:
  L2NormApproximation(const GraphAlignmentProblem & _alignment_problem,
                      const FeatureAlignmentConstraintData & _constraint_data,
                      std::function<void(mosek::fusion::Model::t,
                                         const GraphAlignmentProblem &,
                                         const std::string)> _enforce_transformation_structure,
                      int num_ignored_points,
                      int num_total_sections=8,
                      int num_threads=4,
                      int num_sections_per_thread=1) :
      total_sections(num_total_sections),
      //      subbranch_model_clones(num_total_sections),
      thread_results_mutex(),
      thread_results(num_total_sections),
      //      base_model_mutex(),
      //      base_model(_base_model_pointer->clone()),
      queue_mutex(),
      items_to_process(),
      number_of_threads(num_threads),
      number_of_sections_per_thread(num_sections_per_thread),
      problem(_alignment_problem),
      constraint_data_mutex(),
      constraint_data(_constraint_data),
      enforce_transformation_structure(_enforce_transformation_structure),
      ignored_points(num_ignored_points)
  {
    //    this->set_base_model(base_model_pointer);
  }

  virtual ~L2NormApproximation()
  {
    //    std::cout << "Deleting base model from L2NormApproximation.\n";
    //    this->base_model->dispose();
  }

  SolverResults get_thread_result(int section_number)
  {
    std::lock_guard<std::mutex> guard(thread_results_mutex);
    return this->thread_results[section_number];
  }

  void thread_execution()
  {
    auto subsections = this->get_items_from_queue(this->number_of_sections_per_thread);    
    while(subsections.size() != 0)
    {
      mosek::fusion::Model::t model = new mosek::fusion::Model("ConvexGraphAlignmentProblem");
      //      auto _M  = monty::finally([&]() { std::cout << "Deleting Model from thread.\n";model->dispose(); });
      auto _M  = monty::finally([&]() { model->dispose(); });      

      std::vector<mosek::fusion::Variable::t> all_u;
      for(auto section : subsections)
      {
        //Debug std::cout << "Starting subproblem: " << section << "\n";
        std::string extension = "_"+std::to_string(section);
        {
          std::lock_guard<std::mutex> guard(constraint_data_mutex);
          FeatureAlignmentModelCreator::setup_base_feature_alignment_model(problem,
                                                                           model,
                                                                           constraint_data,
                                                                           extension);
        }
        this->enforce_subproblem_constraints(model, section);
        this->enforce_transformation_structure(model, problem, extension);

        std::string u = std::string("u")+extension;
        //        std::cout << u << std::endl;
        all_u.push_back(model->getVariable(u));
        //        std::cout << "HERE\n" << std::endl;        
      }

      // Set objective
      if(all_u.size() == 1)
      {
        auto obj = mosek::fusion::Expr::sum(all_u[0]);
        model->objective("obj",
                         mosek::fusion::ObjectiveSense::Minimize,
                         obj);      
        //        std::cout << obj->toString()<< "\n";
      }
      else
      {
        auto obj_array = std::make_shared<monty::ndarray<mosek::fusion::Variable::t,1> >(
            all_u.size(), all_u.begin(), all_u.end());
        auto stacked_obj = mosek::fusion::Var::vstack(obj_array);
        auto obj = mosek::fusion::Expr::sum(stacked_obj); 
        model->objective("obj",
                         mosek::fusion::ObjectiveSense::Minimize,
                         obj);       
        //        std::cout << obj->toString()<< "\n";
      }
      
      this->solve_subproblems(model, subsections);

      subsections = this->get_items_from_queue(this->number_of_sections_per_thread);          
    }
  }

  int run()
  {
    for(int i=0;i<this->total_sections;i++)
    {
      this->add_item_to_queue(i);
    }
    
    std::vector<std::thread> threads;

    //Debug std::cout << "[ConvexTransformationEstimation] L2NormApproximation starting threads...\n";
    //Debug std::cout << "Num Threads: " << this->number_of_threads << "\n";
    for (int i=0;i<this->number_of_threads;i++)
    {
      threads.push_back(std::thread(&L2NormApproximation::thread_execution, this));
    }

    //Debug std::cout << "[ConvexTransformationEstimation] L2NormApproximation synchronizing threads...\n";
    for(auto& th : threads)
    {
      th.join();
    }

    //Debug std::cout << "[ConvexTransformationEstimation] L2NormApproximation evaluating best solution...\n";
    double min_value = std::numeric_limits<double>::max();
    int min_section = -1;
    for(int i=0;i<this->total_sections;i++)
    {
      auto result = this->get_thread_result(i);
      if(result.get_solution_value() < min_value)
      {
        min_value = result.get_solution_value();
        min_section = i;
      }
    }

    //Debug std::cout << "[ConvexTransformationEstimation] L2NormApproximation Minimim Section: " << min_section << "\n";
    auto result = this->get_thread_result(min_section);
    //Debug std::cout << "[ConvexTransformationEstimation] L2NormApproximation Minimum Solution Value: " << result.get_solution_value() << "\n";      
    SolverResults::print_unscaled_and_shifted_result(result, this->problem);    
    return min_section;
  }
};

class L2NormApproximation2D: public L2NormApproximation
{
 protected:
  void enforce_subproblem_constraints(mosek::fusion::Model::t model,
                                      int section_number);
  void solve_subproblem(mosek::fusion::Model::t model,
                        int section_number);
  void solve_subproblems(mosek::fusion::Model::t model,
                         std::vector<int> section_numbers);

  
 public:
  L2NormApproximation2D(const GraphAlignmentProblem & alignment_problem,
                        const FeatureAlignmentConstraintData & constraint_data,
                        std::function<void(mosek::fusion::Model::t,
                                           const GraphAlignmentProblem &,
                                           const std::string)>
                        enforce_transformation_structure,                        
                        int num_ignored_points,
                        int num_total_sections=8):
      L2NormApproximation(alignment_problem,
                          constraint_data,
                          enforce_transformation_structure,
                          num_ignored_points,
                          num_total_sections)
  {}
  ~L2NormApproximation2D()
  {
    // Handled automatically
  }
};

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
//
// Transformation Estimation Solvers
//
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Sparse Feature Alignment Solver Base Class
////////////////////////////////////////////////////////////////////////////////

class Solver
{
  // Data Members
 protected:
  int max_iterations;

 private:
  bool has_alternate_solve_sequence;

 public:
  
  // Function Members
 private:

  // This function is called by solve_problem
  // to restrict the domain of the variables
  virtual
  void enforce_transformation_structure(
      mosek::fusion::Model::t model,
      const GraphAlignmentProblem & problem) = 0;

  // This function creates the objective
  // that is minimized
  virtual
  void specify_objective(
      mosek::fusion::Model::t model,
      const GraphAlignmentProblem & problem);
      
  
  // This function is called by solve_problem
  // to process the final results of the optimization
  virtual
  SolverResults process_results(
      mosek::fusion::Model::t model,
      const GraphAlignmentProblem & problem,
      int num_ignored_points);

  virtual
  SolverResults solve_problem(const GraphAlignmentProblem & problem,
                              int iteration_number,
                              double trust_region_size,
                              SolverResults prior_result);
 protected:
  virtual
  void solve_command(mosek::fusion::Model::t model,
                     const GraphAlignmentProblem & problem,
                     const FeatureAlignmentConstraintData & constraint_data,                     
                     int num_ignored_points)
  {
    //Debug std::cout << "[ConvexTransformationEstimation] Begin Solving Problem...\n";
    model->solve();
    //Debug std::cout << "[ConvexTransformationEstimation] Done Solving Problem...\n";
  }
  
 public:
  Solver(int _max_iterations=7):
      max_iterations(_max_iterations),
      has_alternate_solve_sequence(false)
  {}
  virtual ~Solver(){}

  SolverResults solve_problem(const GraphAlignmentProblem & problem);

  void test_mosek();
};


////////////////////////////////////////////////////////////////////////////////
// Affine Transformation Solver
////////////////////////////////////////////////////////////////////////////////

class SolverAffine : public Solver
{
 private:
    void enforce_transformation_structure(
        mosek::fusion::Model::t model,
        const GraphAlignmentProblem & problem);

 public:
  SolverAffine(int _max_iterations=3):
      Solver(_max_iterations)
  {}
  ~SolverAffine(){}
};

////////////////////////////////////////////////////////////////////////////////
// Symmetric Transformation Solver
////////////////////////////////////////////////////////////////////////////////

class SolverSymmetric : public Solver
{
 private:
    void enforce_transformation_structure(
        mosek::fusion::Model::t model,
        const GraphAlignmentProblem & problem);

 public:
  SolverSymmetric(int _max_iterations=3):
      Solver(_max_iterations)
  {}
  ~SolverSymmetric(){}
};

////////////////////////////////////////////////////////////////////////////////
// Convex Hull Based Rigid-body Transformation Solver
////////////////////////////////////////////////////////////////////////////////

class SolverRigidBody_SOConvHull : public Solver
{
 protected:
  bool round_automatically;
  bool clone_model_before_adding_psd_constraint;
  mosek::fusion::Model::t cloned_model;

 private:
  void enforce_transformation_structure(
      mosek::fusion::Model::t model,
      const GraphAlignmentProblem & problem);

  virtual SolverResults process_results(
      mosek::fusion::Model::t model,
      const GraphAlignmentProblem & problem,
      int num_ignored_points);

 public:
  SolverRigidBody_SOConvHull(int _max_iterations=3,
                             bool _round_automatically=false):
      Solver(_max_iterations),
      round_automatically(_round_automatically),
      clone_model_before_adding_psd_constraint(false),
      cloned_model()
  {}
  ~SolverRigidBody_SOConvHull()
  {
    this->cloned_model->dispose();
  }
};


////////////////////////////////////////////////////////////////////////////////
// Linear Approx Based Rigid-body Transformation Solver
////////////////////////////////////////////////////////////////////////////////

class SolverRigidBody_L2LinearApprox : public Solver
{
 private:
  bool round_automatically;
  int best_section;
  SolverResults best_result;

  void enforce_transformation_structure(
      mosek::fusion::Model::t model,
      const GraphAlignmentProblem & problem)
  {
    this->enforce_transformation_structure(model, problem, "");
  }
     
  void enforce_transformation_structure(
      mosek::fusion::Model::t model,
      const GraphAlignmentProblem & problem,
      const std::string variable_extension="");
  
  SolverResults process_results(
      const GraphAlignmentProblem & problem,
      int num_ignored_points);

  SolverResults solve_problem(const GraphAlignmentProblem & problem,
                              int iteration_number,
                              double trust_region_size,
                              SolverResults prior_result);  

 public:
  SolverRigidBody_L2LinearApprox(int _max_iterations=3,
                                 bool _round_automatically=false):
      Solver(_max_iterations),
      round_automatically(_round_automatically),
      best_section(-1),
      best_result()
  {}

  ~SolverRigidBody_L2LinearApprox(){}

  SolverResults solve_problem(const GraphAlignmentProblem & problem);
};

////////////////////////////////////////////////////////////////////////////////
// Default Rigid-Body Transformation Solver
////////////////////////////////////////////////////////////////////////////////
typedef SolverRigidBody_L2LinearApprox SolverRigidBody;

} // Namespace ConvexTransformationEstimation

#endif // __CONVEX_TRANSFORMATION_ESTIMATION_HPP 
 
